BUILD:
docker build -t gonzalovarela/todo-frontend .

RUN:
docker run -it -p 8080:80 --rm --name todo-frontend-app gonzalovarela/todo-frontend

COMPOSE:
docker-compose -p todo up -d
