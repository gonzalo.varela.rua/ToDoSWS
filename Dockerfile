# Compilación

FROM node:18.4.0-alpine as build
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
# Para que funcione el anterior comando ha sido necesario
# especificar la versión de 'esbuild' en 'packages.json'
#
# "esbuild": "0.14.46"

# Producción

FROM nginx:1.23.0-alpine as production
COPY --from=build /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]